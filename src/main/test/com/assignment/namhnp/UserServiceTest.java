package com.assignment.namhnp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;

import com.assignment.namhnp.model.User;
import com.assignment.namhnp.repository.UserRepository;
import com.assignment.namhnp.serviceImpl.UserDetailServiceImpl;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceTest {
	@InjectMocks
	private UserDetailServiceImpl userDetailService;

	@Mock
	private UserRepository userRepository;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testLoadUserByUserNameSuccess() throws Exception {
		User user = new User();
		user.setPassword("namhnp");
		user.setUsername("namhnp");
		user.setUserId(1);

		when(userRepository.getByUsername(user.getUsername())).thenReturn(user);

		UserDetails userDetailsResponse = userDetailService.loadUserByUsername(user.getUsername());

		Assert.assertEquals(user.getUsername(), userDetailsResponse.getUsername());
		Assert.assertEquals(user.getPassword(), userDetailsResponse.getPassword());
	}
	
	@Test
    public void testLoadUserByUserName() throws Exception {
        User user = new User();
        user.setPassword("namhnp");
        user.setUsername("namhnp");
        user.setUserId(1);

        when(userRepository.getByUsername(user.getUsername())).thenReturn(user);

        UserDetails userDetailsResponse = userDetailService.loadUserByUsername(user.getUsername());

        Assert.assertEquals(user.getUsername(), userDetailsResponse.getUsername());
        Assert.assertEquals(user.getPassword(), userDetailsResponse.getPassword());
    }

}
