package com.assignment.namhnp.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.namhnp.dto.UserFileRoleRequestDto;
import com.assignment.namhnp.dto.UserFileRoleResponseDto;
import com.assignment.namhnp.service.UserFileRoleService;

@RestController
@RequestMapping("/api/v1")
public class RoleController {
	@Autowired
	UserFileRoleService userFileRoleService;
	
	@PostMapping
	public ResponseEntity<?> grantRole(@RequestBody UserFileRoleRequestDto userFileRoleRequestDto){
		
		return null;
	}
	
	@GetMapping(value = "/userFileRole/{fileId}")
	public ResponseEntity<Set<UserFileRoleResponseDto>> getAllUserWithFileId(@PathVariable("fileId") int fileId){
		Set<UserFileRoleResponseDto> userFileRoleResponseDtos = new HashSet<UserFileRoleResponseDto>();
		userFileRoleResponseDtos = userFileRoleService.getAllUserWithFileId(fileId);
		userFileRoleResponseDtos.addAll(userFileRoleService.getAllUserNonWithFileId(fileId));
		return new ResponseEntity<Set<UserFileRoleResponseDto>>(userFileRoleResponseDtos, HttpStatus.OK);
	}
}
