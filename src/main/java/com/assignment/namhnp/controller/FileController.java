package com.assignment.namhnp.controller;

import com.assignment.namhnp.dto.FileUploadResponseDto;
import com.assignment.namhnp.model.UserFileRole;
import com.assignment.namhnp.model.UserLoginContext;
import com.assignment.namhnp.repository.UserFileRoleRepository;
import com.assignment.namhnp.service.FileService;
import com.assignment.namhnp.service.UserFileRoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.assignment.namhnp.constants.Constants.FILE_NOT_FOUND;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/files")
public class FileController {
	@Autowired
	private FileService fileService;

	@Autowired
	private UserFileRoleService userFileRoleService;

	@SuppressWarnings("unchecked")
	@PostMapping
	public ResponseEntity<FileUploadResponseDto> uploadFile(@RequestParam("file") MultipartFile file) {
		return (ResponseEntity<FileUploadResponseDto>) fileService.storeFile(file);
	}

	@GetMapping(value = "/{userId}/download/{fileId}")
	public ResponseEntity<?> downloadFile(@PathVariable("userId") int userId, @PathVariable("fileId") int fileId,
			HttpServletRequest request) {
		FileUploadResponseDto fileUploadResponseDto = new FileUploadResponseDto();

		if (fileService.checkFileExist(userId, fileId)
				&& userFileRoleService.checkPermissionToDownload(userId, fileId)) {
			try {
				Resource resource = fileService.downloadFile(userId, fileId);
				String contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());

				if (contentType == null) {
					contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
				}

				return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
						.header(HttpHeaders.CONTENT_DISPOSITION,
								"attachment; filename=\"" + resource.getFilename() + "\"")
						.body(resource);
			} catch (Exception e) {
				fileUploadResponseDto.setMessage(FILE_NOT_FOUND + fileId);
				return new ResponseEntity<>(fileUploadResponseDto, HttpStatus.NOT_FOUND);
			}
		} else {
			fileUploadResponseDto.setMessage("FILE_NOT_FOUND");
			return new ResponseEntity<>(fileUploadResponseDto, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/all")
	public ResponseEntity<?> getAllFileOfUser() {
		return new ResponseEntity<>(fileService.getListFileOfUser(), HttpStatus.OK);
	}

	@DeleteMapping("{userId}/delete/{fileId}")
	public ResponseEntity<FileUploadResponseDto> deleteFile(@PathVariable("userId") int userId, @PathVariable("fileId") int fileId) {
		FileUploadResponseDto response = new FileUploadResponseDto();
		if (userFileRoleService.checkPermissionToDelete(userId, fileId)) {
			if (fileService.deleteFile(userId, fileId)) {
				System.out.println("Vao day");
				response.setMessage("Delete Success");
				return new ResponseEntity<>(response, HttpStatus.OK);	
			} else {
				response.setMessage("Delete Failed");
				System.out.println("Vao day 2");
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);	
			}
			
		} else {
			System.out.println("Vao day3");
			response.setMessage("Permission Denied");
			return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
		}

	}
}
