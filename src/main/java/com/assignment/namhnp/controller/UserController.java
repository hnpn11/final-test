package com.assignment.namhnp.controller;

import com.assignment.namhnp.dto.UserFileRoleResponseDto;
import com.assignment.namhnp.dto.UserRequestDto;
import com.assignment.namhnp.model.User;
import com.assignment.namhnp.service.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/users")
    public ResponseEntity<User> createNewUser(@RequestBody UserRequestDto loginRequestDto) {
        User user = userService.createUser(loginRequestDto);

        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
    
}
