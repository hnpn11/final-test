package com.assignment.namhnp.controller;

import com.assignment.namhnp.dto.UserRequestDto;
import com.assignment.namhnp.dto.UserResponseDto;
import com.assignment.namhnp.model.User;
import com.assignment.namhnp.repository.UserRepository;
import com.assignment.namhnp.serviceImpl.UserDetailServiceImpl;
import com.assignment.namhnp.utils.JwtTokenUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1")
public class AuthenticationController {
    @Autowired
    private JwtTokenUtils jwtTokenUtils;

    @Autowired
    private UserDetailServiceImpl userDetailsService;

    @Autowired
    protected AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @PostMapping(value = "/login", consumes = MediaType.ALL_VALUE)
    public ResponseEntity<UserResponseDto> authenUser(@RequestBody UserRequestDto loginRequestDto) {
        authenticate(loginRequestDto.getUsername(), loginRequestDto.getPassword());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(loginRequestDto.getUsername());
        final String token = jwtTokenUtils.generateToken(userDetails);

        return ResponseEntity.ok(new UserResponseDto(token));
    }

    private void authenticate(String userName, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
    }
}
