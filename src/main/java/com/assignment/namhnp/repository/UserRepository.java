package com.assignment.namhnp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.assignment.namhnp.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User getByUsername(String username);

    @Query("SELECT u from User u")
	List<User> getAll();
}
