package com.assignment.namhnp.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.assignment.namhnp.dto.UserFileRoleResponseDto;
import com.assignment.namhnp.model.UserFileId;
import com.assignment.namhnp.model.UserFileRole;

public interface UserFileRoleRepository extends JpaRepository<UserFileRole, UserFileId> {

	@Query("SELECT f from UserFileRole f where f.userFileId.user.userId = :userId")
	List<UserFileRole> getUserFileRoleByUserId(int userId);

	@Query("SELECT f from UserFileRole f WHERE f.userFileId.user.userId = :userId AND f.userFileId.fileManagement.fileId = :fileId")
	UserFileRole getUserFileRoleByUserIdAndFileId(int userId, int fileId);

	@Query("SELECT f.userFileId.user.userId, f.userFileId.user.username, f.haveOwner, f.haveShare, f.haveRead from UserFileRole f WHERE f.userFileId.fileManagement.fileId = :fileId")
	Set<UserFileRoleResponseDto> getAllUserWithFileId(int fileId);
	
	@Query("SELECT f.userFileId.user.userId, f.userFileId.user.username, f.haveOwner, f.haveShare, f.haveRead from UserFileRole f WHERE f.userFileId.fileManagement.fileId != :fileId")
	Set<UserFileRoleResponseDto> getAllUserNonWithFileId(int fileId);
}
