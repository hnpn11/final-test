package com.assignment.namhnp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.assignment.namhnp.model.FileManagement;
import com.assignment.namhnp.model.User;

import java.lang.annotation.Native;
import java.util.List;

public interface FileRepository extends JpaRepository<FileManagement, Integer> {
	
    @Query("SELECT f FROM FileManagement f WHERE f.filePath = :filePath")
    FileManagement getFileManagementByFilePath(String filePath);

	FileManagement getFileManagementByFileId(int fileId);
}
