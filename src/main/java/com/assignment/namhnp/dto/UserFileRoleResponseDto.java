package com.assignment.namhnp.dto;

public class UserFileRoleResponseDto {
	private int userId;
	private String username;
	private boolean haveOwner;
	private boolean haveShare;
	private boolean havaRead;

	public UserFileRoleResponseDto(int userId, String username, boolean haveOwner, boolean haveShare,
			boolean havaRead) {
		super();
		this.userId = userId;
		this.username = username;
		this.haveOwner = haveOwner;
		this.haveShare = haveShare;
		this.havaRead = havaRead;
	}

	public UserFileRoleResponseDto() {
		super();
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isHaveOwner() {
		return haveOwner;
	}

	public void setHaveOwner(boolean haveOwner) {
		this.haveOwner = haveOwner;
	}

	public boolean isHaveShare() {
		return haveShare;
	}

	public void setHaveShare(boolean haveShare) {
		this.haveShare = haveShare;
	}

	public boolean isHavaRead() {
		return havaRead;
	}

	public void setHavaRead(boolean havaRead) {
		this.havaRead = havaRead;
	}

}
