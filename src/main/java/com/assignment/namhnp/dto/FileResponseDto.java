package com.assignment.namhnp.dto;

import java.util.List;

public class FileResponseDto {
	List<FileManagementDto> fileManagementDtos;

	public FileResponseDto() {
		super();
	}

	public FileResponseDto(List<FileManagementDto> fileManagementDtos) {
		super();
		this.fileManagementDtos = fileManagementDtos;
	}

	public List<FileManagementDto> getData() {
		return fileManagementDtos;
	}

	public void setData(List<FileManagementDto> fileManagementDtos) {
		this.fileManagementDtos = fileManagementDtos;
	}
}
