package com.assignment.namhnp.dto;

public class FileUploadResponseDto {
	private String message;

	public FileUploadResponseDto() {
		super();
	}

	public FileUploadResponseDto(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
