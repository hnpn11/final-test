package com.assignment.namhnp.dto;

public class UserResponseDto {
    private String tokenId;

    public UserResponseDto(String tokenId) {
        this.tokenId = "Bearer " + tokenId;
    }

    public String getTokenId() {
        return tokenId;
    }
}
