package com.assignment.namhnp.dto;

public class FileManagementDto {
	private int fileId;
	private int userId;
	private String fileName;
	private boolean haveOwner;
	private boolean haveShare;
	private boolean haveRead;

	public FileManagementDto() {
		super();
	}

	public FileManagementDto(int fileId, int userId, String fileName, boolean haveOwner, boolean haveShare,
			boolean haveRead) {
		super();
		this.fileId = fileId;
		this.userId = userId;
		this.fileName = fileName;
		this.haveOwner = haveOwner;
		this.haveShare = haveShare;
		this.haveRead = haveRead;
	}

	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isHaveOwner() {
		return haveOwner;
	}

	public void setHaveOwner(boolean haveOwner) {
		this.haveOwner = haveOwner;
	}

	public boolean isHaveShare() {
		return haveShare;
	}

	public void setHaveShare(boolean haveShare) {
		this.haveShare = haveShare;
	}

	public boolean isHaveRead() {
		return haveRead;
	}

	public void setHaveRead(boolean haveRead) {
		this.haveRead = haveRead;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
