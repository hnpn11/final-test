package com.assignment.namhnp.dto;

public class UserFileRoleRequestDto {

	private int userId;
	private int fileId;
	private boolean haveOwner;
	private boolean haveShare;
	private boolean haveRead;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public boolean isHaveOwner() {
		return haveOwner;
	}

	public void setHaveOwner(boolean haveOwner) {
		this.haveOwner = haveOwner;
	}

	public boolean isHaveShare() {
		return haveShare;
	}

	public void setHaveShare(boolean haveShare) {
		this.haveShare = haveShare;
	}

	public boolean isHaveRead() {
		return haveRead;
	}

	public void setHaveRead(boolean haveRead) {
		this.haveRead = haveRead;
	}

	public UserFileRoleRequestDto(int userId, int fileId, boolean haveOwner, boolean haveShare, boolean haveRead) {
		super();
		this.userId = userId;
		this.fileId = fileId;
		this.haveOwner = haveOwner;
		this.haveShare = haveShare;
		this.haveRead = haveRead;
	}

	public UserFileRoleRequestDto() {
		super();
	}

}
