package com.assignment.namhnp.model;

import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name = "files")
public class FileManagement {
	@Id
	@Column(name = "file_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int fileId;

	@Column(name = "file_name")
	private String fileName;

	@Column(name = "file_path")
	private String filePath;

	@OneToMany(mappedBy = "userFileId.fileManagement", cascade = CascadeType.ALL)
	private Set<UserFileRole> userFileRoles;

	public FileManagement() {
		super();
	}

	public FileManagement(int fileId, String fileName, String filePath) {
		super();
		this.fileId = fileId;
		this.fileName = fileName;
		this.filePath = filePath;
	}

	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Set<UserFileRole> getUserFileRoles() {
		return userFileRoles;
	}

	public void setUserFileRoles (Set<UserFileRole> userFileRoles) {
		this.userFileRoles = userFileRoles;
	}

}
