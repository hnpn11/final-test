package com.assignment.namhnp.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Nam Ha
 *
 */
@Entity
@Table(name = "user_file_role")
public class UserFileRole {
	@EmbeddedId
	private UserFileId userFileId;

	@Column(name = "HaveOwner")
	private boolean haveOwner;

	@Column(name = "HaveShare")
	private boolean haveShare;

	@Column(name = "HaveRead")
	private boolean haveRead;

	public UserFileRole(UserFileId userFileId, boolean haveOwner, boolean haveShare,
			boolean haveRead) {
		super();
		userFileId = userFileId;
		this.haveOwner = haveOwner;
		this.haveShare = haveShare;
		this.haveRead = haveRead;
	}

	public UserFileRole() {
		super();
	}

	public UserFileId getUserFileId() {
		return userFileId;
	}

	public void setUserFileId(UserFileId userFileId) {
		this.userFileId = userFileId;
	}

	public boolean isHaveOwner() {
		return haveOwner;
	}

	public void setHaveOwner(boolean haveOwner) {
		this.haveOwner = haveOwner;
	}

	public boolean isHaveShare() {
		return haveShare;
	}

	public void setHaveShare(boolean haveShare) {
		this.haveShare = haveShare;
	}

	public boolean isHaveRead() {
		return haveRead;
	}

	public void setHaveRead(boolean haveRead) {
		this.haveRead = haveRead;
	}
	

}
