package com.assignment.namhnp.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Embeddable
public class UserFileId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManyToOne()
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToOne()
	@JoinColumn(name = "file_id")
	private FileManagement fileManagement;

	public UserFileId(User user, FileManagement fileManagement) {
		super();
		this.user = user;
		this.fileManagement = fileManagement;
	}

	public UserFileId() {
		super();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public FileManagement getFileManagement() {
		return fileManagement;
	}

	public void setFileManagement(FileManagement fileManagement) {
		this.fileManagement = fileManagement;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fileManagement == null) ? 0 : fileManagement.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserFileId other = (UserFileId) obj;
		if (fileManagement == null) {
			if (other.fileManagement != null)
				return false;
		} else if (!fileManagement.equals(other.fileManagement))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	
}
