package com.assignment.namhnp.model;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private int userId;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@OneToMany(mappedBy = "userFileId.user")
	private Set<UserFileRole> userFileRoles;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<UserFileRole> getUserFileRoles() {
		return userFileRoles;
	}

	public void setUserFileRoles(Set<UserFileRole> userFileRoles) {
		this.userFileRoles = userFileRoles;
	}

}
