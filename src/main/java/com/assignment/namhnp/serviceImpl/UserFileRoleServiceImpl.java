package com.assignment.namhnp.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.namhnp.dto.UserFileRoleResponseDto;
import com.assignment.namhnp.model.FileManagement;
import com.assignment.namhnp.model.User;
import com.assignment.namhnp.model.UserFileRole;
import com.assignment.namhnp.repository.FileRepository;
import com.assignment.namhnp.repository.UserFileRoleRepository;
import com.assignment.namhnp.service.UserFileRoleService;

@Service
public class UserFileRoleServiceImpl implements UserFileRoleService {

	@Autowired
	UserFileRoleRepository userFileRoleRepository;

	@Autowired
	FileRepository fileRepository;

	@Override
	public boolean checkPermissionToDownload(int userId, int fileId) {
		UserFileRole userFileRole = userFileRoleRepository.getUserFileRoleByUserIdAndFileId(userId, fileId);
		return userFileRole.isHaveRead();
	}

	@Override
	public boolean checkPermissionToDelete(int userId, int fileId) {
		UserFileRole userFileRole = userFileRoleRepository.getUserFileRoleByUserIdAndFileId(userId, fileId);
		if (userFileRole != null) {
			return userFileRole.isHaveOwner();
		}
		return false;
	}

	@Override
	public Set<UserFileRoleResponseDto> getAllUserWithFileId(int fileId) {
		Set<UserFileRoleResponseDto> userRoleResponseDtos = userFileRoleRepository.getAllUserWithFileId(fileId);
		return userRoleResponseDtos;
	}
	
	@Override
	public Set<UserFileRoleResponseDto> getAllUserNonWithFileId(int fileId) {
		Set<UserFileRoleResponseDto> userRoleResponseDtos = userFileRoleRepository.getAllUserNonWithFileId(fileId);
		return userRoleResponseDtos;
	}
}
