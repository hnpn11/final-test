package com.assignment.namhnp.serviceImpl;

import com.assignment.namhnp.dto.FileUploadResponseDto;
import com.assignment.namhnp.dto.FileManagementDto;
import com.assignment.namhnp.dto.FileResponseDto;
import com.assignment.namhnp.model.FileManagement;
import com.assignment.namhnp.model.User;
import com.assignment.namhnp.model.UserFileId;
import com.assignment.namhnp.model.UserFileRole;
import com.assignment.namhnp.model.UserLoginContext;
import com.assignment.namhnp.repository.FileRepository;
import com.assignment.namhnp.repository.UserFileRoleRepository;
import com.assignment.namhnp.repository.UserRepository;
import com.assignment.namhnp.service.FileService;
import com.assignment.namhnp.service.UserFileRoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.handler.UserRoleAuthorizationInterceptor;

import static com.assignment.namhnp.constants.Constants.FILE_NOT_FOUND;
import static com.assignment.namhnp.constants.Constants.USER_HOME;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class FileServiceImpl implements FileService {
	@Value("${file.upload-dir}")
	private String uploadDir;

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserFileRoleRepository userFileRoleRepository;
	
	@Autowired
	private UserFileRoleService userFileRoleService;

	/**
	 * Function store file to directory on server and insert file name into
	 * database.
	 * 
	 * @param file
	 * @return
	 */
	@Override
	public ResponseEntity<FileUploadResponseDto> storeFile(MultipartFile file) {
		String storeDirectory = System.getProperty(USER_HOME) + File.separator + uploadDir + File.separator
				+ UserLoginContext.getUserName();
		String filePath = storeDirectory + File.separator + Objects.requireNonNull(file.getOriginalFilename());
		FileUploadResponseDto fileUploadResponseDto = new FileUploadResponseDto();
		UserFileId userFileId = new UserFileId();
		UserFileRole userFileRole = new UserFileRole();
		try {
			Files.createDirectories(Paths.get(storeDirectory));
			Path fileUpload = Paths.get(storeDirectory).resolve(Objects.requireNonNull(file.getOriginalFilename()));
			Files.copy(file.getInputStream(), fileUpload, StandardCopyOption.REPLACE_EXISTING);

			FileManagement fileManagement = fileRepository.getFileManagementByFilePath(filePath);
			if (fileManagement != null) {
				fileUploadResponseDto.setMessage("File is existed");
				return new ResponseEntity<>(fileUploadResponseDto, HttpStatus.CONFLICT);
			} else {
				fileManagement = new FileManagement();
				fileManagement.setFileName(Objects.requireNonNull(file.getOriginalFilename()));
				fileManagement.setFilePath(filePath);
				fileManagement = fileRepository.save(fileManagement);

				User user = new User();
				user.setUserId(UserLoginContext.getUserDid());

				userFileId.setUser(user);
				userFileId.setFileManagement(fileManagement);
				userFileRole.setUserFileId(userFileId);
				userFileRole.setHaveOwner(true);
				userFileRole.setHaveRead(true);
				userFileRole.setHaveShare(true);
				Set<UserFileRole> userFileRoles = new HashSet<UserFileRole>();
				user.setUserFileRoles(userFileRoles);
				fileManagement.setUserFileRoles(userFileRoles);
				userFileRoleRepository.save(userFileRole);
				fileUploadResponseDto.setMessage("Upload file successfully");
				return new ResponseEntity<>(fileUploadResponseDto, HttpStatus.OK);
			}
		} catch (IOException ex) {
			fileUploadResponseDto.setMessage("Failed upload with file name: " + file.getName());
			return new ResponseEntity<>(fileUploadResponseDto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Function get file from service with file name and user id, if file not exist,
	 * throw exception.
	 * 
	 * @param userId
	 * @param fileName
	 * @return resource
	 * @throws Exception
	 */
	@Override
	public Resource downloadFile(int userId, int fileId) throws Exception {
		String storeDirectory = System.getProperty(USER_HOME) + File.separator + uploadDir + File.separator
				+ UserLoginContext.getUserName();

		FileManagement fileManagement = fileRepository.getFileManagementByFileId(fileId);

		String fileUri = storeDirectory + File.separator + fileManagement.getFileName();

		try {
			Resource resource = new UrlResource(Paths.get(fileUri).toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new FileNotFoundException(FILE_NOT_FOUND + fileUri);
			}
		} catch (MalformedURLException e) {
			throw new FileNotFoundException(FILE_NOT_FOUND + fileUri);
		}
	}

	@Override
	public FileResponseDto getListFileOfUser() {
		User user = new User();
		user.setUserId(UserLoginContext.getUserDid());
		List<UserFileRole> userFileRoles = userFileRoleRepository.getUserFileRoleByUserId(user.getUserId());
		List<FileManagementDto> fileManagementDtos = new ArrayList<>();
		for (UserFileRole userFileRole : userFileRoles) {
			FileManagementDto fileManagementDto = new FileManagementDto();
			fileManagementDto.setFileId(userFileRole.getUserFileId().getFileManagement().getFileId());
			fileManagementDto.setUserId(userFileRole.getUserFileId().getUser().getUserId());
			fileManagementDto.setFileName(userFileRole.getUserFileId().getFileManagement().getFileName());
			fileManagementDto.setHaveOwner(userFileRole.isHaveOwner());
			fileManagementDto.setHaveRead(userFileRole.isHaveRead());
			fileManagementDto.setHaveRead(userFileRole.isHaveRead());
			fileManagementDtos.add(fileManagementDto);
		}

		FileResponseDto fileResponseDto = new FileResponseDto();
		fileResponseDto.setData(fileManagementDtos);
		return fileResponseDto;
	}

	@Override
	public boolean checkFileExist(int userId, int fileId) {
		UserFileRole userFileRole = userFileRoleRepository.getUserFileRoleByUserIdAndFileId(userId, fileId);
		if (userFileRole != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean deleteFile(int userId, int fileId) {
		if (userFileRoleService.checkPermissionToDelete(userId, fileId)) {
			fileRepository.deleteById(fileId);
			FileManagement fileManagement = fileRepository.getFileManagementByFileId(fileId);
			if (fileManagement != null) {
				return false;
			}
			return true;
		}
		return false;
	}

}
