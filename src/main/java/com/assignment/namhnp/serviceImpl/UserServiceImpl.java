package com.assignment.namhnp.serviceImpl;

import com.assignment.namhnp.dto.UserFileRoleResponseDto;
import com.assignment.namhnp.dto.UserRequestDto;
import com.assignment.namhnp.model.User;
import com.assignment.namhnp.repository.UserRepository;
import com.assignment.namhnp.service.UserFileRoleService;
import com.assignment.namhnp.service.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.handler.UserRoleAuthorizationInterceptor;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserFileRoleService userFileRoleService;

    @Override
    public User createUser(UserRequestDto requestDto) {
        User user = new User();
        user.setUsername(requestDto.getUsername());

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        user.setPassword(passwordEncoder.encode(requestDto.getPassword()));

        User createdUser = userRepository.save(user);
        return createdUser;
    }

}
