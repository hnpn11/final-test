package com.assignment.namhnp.serviceImpl;

import com.assignment.namhnp.model.User;
import com.assignment.namhnp.model.UserLoginContext;
import com.assignment.namhnp.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.getByUsername(s);
        if (user != null) {
            UserLoginContext.setUserDid(user.getUserId());
            UserLoginContext.setUserName(user.getUsername());
            return new org.springframework.security.core.userdetails.User
                    (user.getUsername(), user.getPassword(), new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found");
        }
    }
}
