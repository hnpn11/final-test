package com.assignment.namhnp.service;

import java.util.List;

import com.assignment.namhnp.dto.UserFileRoleResponseDto;
import com.assignment.namhnp.dto.UserRequestDto;
import com.assignment.namhnp.model.User;

public interface UserService {
    User createUser(UserRequestDto requestDto);
}
