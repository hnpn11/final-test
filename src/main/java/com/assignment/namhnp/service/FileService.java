package com.assignment.namhnp.service;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.assignment.namhnp.dto.FileResponseDto;

public interface FileService {
    ResponseEntity<?> storeFile(MultipartFile file);
    Resource downloadFile(int userId, int fileId) throws Exception;
    FileResponseDto getListFileOfUser();
	boolean checkFileExist(int userId, int fileId);
	boolean deleteFile(int userId, int fileId);
}
