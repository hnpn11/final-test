package com.assignment.namhnp.service;

import java.util.List;
import java.util.Set;

import com.assignment.namhnp.dto.UserFileRoleResponseDto;

public interface UserFileRoleService {

	boolean checkPermissionToDownload(int userId, int fileId);

	boolean checkPermissionToDelete(int userId, int fileId);

	Set<UserFileRoleResponseDto> getAllUserWithFileId(int fileId);
	
	Set<UserFileRoleResponseDto> getAllUserNonWithFileId(int fileId);

}
